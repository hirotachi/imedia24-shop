A simple restful api for imedia24 shop.

To run project in docker first it needs to be packaged with maven. To do this run:

1- build application jar
```bash
mvn clean package
```

2- build docker container:

```bash 
docker build -t my-app .
```

3- run docker container:

```bash
docker run -p 8080:8080 my-app
```


To run tests:

```bash
mvn test
```