package de.imedia24.shop.db.respository

import de.imedia24.shop.db.entity.Product
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository

@Repository
interface ProductRepository : CrudRepository<Product, String> {

    fun findBySku(sku: String): Product?
}
