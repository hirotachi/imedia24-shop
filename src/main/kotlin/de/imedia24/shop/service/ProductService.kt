package de.imedia24.shop.service

import de.imedia24.shop.db.entity.Product
import de.imedia24.shop.db.respository.ProductRepository
import de.imedia24.shop.domain.product.ProductResponse
import de.imedia24.shop.domain.product.ProductResponse.Companion.toProductResponse
import org.springframework.stereotype.Service
import java.math.BigDecimal

@Service
class ProductService(private val productRepository: ProductRepository) {
    val allowedUpdateKeys = listOf("name", "description", "price", "stock")
    fun findProductBySku(sku: String): ProductResponse? {
        return findBySku(sku)?.toProductResponse()
    }

    fun findBySku(sku: String): Product? {
        return productRepository.findBySku(sku)
    }

    fun saveProduct(product: Product): ProductResponse? {
        return productRepository.save(product).toProductResponse()
    }

    fun findAllBySku(skus: List<String>): List<ProductResponse> {
        if (skus.isEmpty()) {
            throw IllegalArgumentException("The list of SKUs should not be empty")
        }

        return productRepository.findAllById(skus.distinct()).map { it.toProductResponse() }
    }

    fun findAndUpdateProduct(sku: String, updates: Map<String, Any>): ProductResponse? {
        // Validate input
        if (sku.isNullOrEmpty()) {
            throw IllegalArgumentException("sku must not be null or empty")
        }
        if (updates.keys.any { it !in allowedUpdateKeys }) {
            throw IllegalArgumentException("Only the following keys are allowed to be updated: $allowedUpdateKeys")
        }

        val product = findBySku(sku) ?: throw IllegalArgumentException("Product with sku '$sku' not found")

        var updatedProduct = product.copy(
            name = updates["name"] as? String ?: product.name,
            description = updates["description"] as? String ?: product.description,
        )

        listOf("price", "stock").forEach { key ->
            if (updates.containsKey(key)) {
                var value = updates[key]
                if (!value.toString().matches(Regex("\\d+(\\.\\d+)?"))) {
                    throw IllegalArgumentException("Value for $key must be a number")
                }
                updatedProduct = updatedProduct.copy(
                    price = if (key == "price") BigDecimal(value.toString()) else product.price,
                    stock = if (key == "stock") value.toString().toInt() else product.stock
                )
            }
        }

        return try {
            saveProduct(updatedProduct)
        } catch (e: Exception) {
            throw Exception("Error updating product: ${e.message}")
        }
    }

}
