package de.imedia24.shop.domain.product

import de.imedia24.shop.db.entity.Product
import java.math.BigDecimal

data class ProductDto(
    var sku: String,
    var name: String,
    var description: String? = null,
    var price: BigDecimal,
    var stock: Int
) {
    fun toProduct() = Product(
        sku = sku,
        name = name,
        description = description,
        price = price,
        stock = stock
    )

}