package de.imedia24.shop.domain.product

import de.imedia24.shop.db.entity.Product
import java.math.BigDecimal

data class ProductResponse(
    val sku: String,
    val name: String,
    val description: String,
    val price: BigDecimal,
    val stock: Int
) {
    companion object {
        fun Product.toProductResponse() = ProductResponse(
            sku = sku,
            name = name,
            description = description ?: "",
            price = price,
            stock = stock
        )
    }
}

