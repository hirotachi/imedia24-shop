package de.imedia24.shop.controllers

import de.imedia24.shop.db.entity.Product
import de.imedia24.shop.domain.product.ProductResponse.Companion.toProductResponse
import de.imedia24.shop.service.ProductService
import org.junit.jupiter.api.Test
import org.mockito.Mockito.`when`
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.content
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status

@SpringBootTest
@AutoConfigureMockMvc
class ProductControllerTest(@Autowired private val mockMvc: MockMvc) {

    @MockBean
    private val productService: ProductService? = null

    @Test
    fun findProductsBySku() {
        val sku = "123"
        val product = Product(sku, "Product 1", "Description 1", 10.0.toBigDecimal(), 10)
        `when`(productService?.findProductBySku(sku)).thenReturn(product.toProductResponse())

        mockMvc.perform(get("/products/$sku")).andExpect(status().isOk).andExpect(
                content().json(
                    """
                {
                    "sku": "123",
                    "name": "Product 1",
                    "description": "Description 1",
                    "price": 10.0,
                    "stock": 10
                }
            """.trimIndent()
                )
            )
    }

    @Test
    fun createProduct() {
        val sku = "123"
        val product = Product(sku, "Product 1", "Description 1", 10.0.toBigDecimal(), 10)
        `when`(productService?.saveProduct(product)).thenReturn(product.toProductResponse())

        val body = """
                {
                    "sku": "123",
                    "name": "Product 1",
                    "description": "Description 1",
                    "price": 10.0,
                    "stock": 10
                }
            """.trimIndent()
        mockMvc.perform(
            post("/products").contentType(MediaType.APPLICATION_JSON).content(
                    body
                )
        ).andExpect(status().isCreated)
    }

    @Test
    fun getProducts() {
        val sku = "123"
        val product = Product(sku, "Product 1", "Description 1", 10.0.toBigDecimal(), 10)
        `when`(productService?.findAllBySku(listOf(sku))).thenReturn(listOf(product.toProductResponse()))
        mockMvc.perform(get("/products?skus=$sku")).andExpect(status().isOk).andExpect(
                content().json(
                    """
                [
                    {
                        "sku": "123",
                        "name": "Product 1",
                        "description": "Description 1",
                        "price": 10.0,
                        "stock": 10
                    }
                ]
            """.trimIndent()
                )
            )
    }

    @Test
    fun updateProduct() {
        val sku = "123"
        val product = Product(sku, "Product 1", "Description 1", 10.0.toBigDecimal(), 10)
        val updatedProduct = product.copy(name = "Updated Product 1")
        val updates = mapOf("name" to "Updated Product 1")

        `when`(productService?.findBySku(sku)).thenReturn(product)
        `when`(productService?.findAndUpdateProduct(sku, updates)).thenReturn(updatedProduct.toProductResponse())

        val body = """
                {
                    "name": "Updated Product 1"
                }
            """.trimIndent()
        mockMvc.perform(
            patch("/products/$sku").contentType(MediaType.APPLICATION_JSON).content(
                    body
                )
        ).andExpect(status().isOk).andExpect(
                content().json(
                    """
                {
                    "sku": "123",
                    "name": "Updated Product 1",
                    "description": "Description 1",
                    "price": 10.0,
                    "stock": 10
                }
            """.trimIndent()
                )
            )
    }
}